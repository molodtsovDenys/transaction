function openClientList() {

    $('#main-content').load(
        '/v1/clients #clients-main > *',
        function () {
            new ClientList();
        }
    );

}

function ClientList() {

    let
        clientTable = initTable($("#clients-tbl"),
            {
                processing: true,
                serverSide: true,
                ordering: true,
                ajax: {
                    type: "GET",
                    url: "/api/v1/clients",
                    dataType: "json",
                    data: function (data) {
                        data.searchValue = data.search.value;
                        return data;
                    }
                },
                columns: [
                    {data: "firstName"},
                    {data: "lastName"},
                    {data: "middleName"},
                    {data: "inn"}

                ]
            }
        ),

        appendClientInfo = (data) => {
            $("#client-info").text(
                " INN: " + data.inn
            );
        },

        rowHandler = () => {

            $('#clients-tbl tbody').on('click', 'tr', function () {

                $("#transactions").empty();

                let data = clientTable.row(this).data();

                let inn = data.inn;

                uploadTransactionList(inn);

                appendClientInfo(data);
            });
        },


        initUI = function () {
            rowHandler();
            $("#transactions").empty();
        };

    initUI();

};






