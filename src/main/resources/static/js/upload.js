function FilesUploadForm(jq) {
    var
        $jq = jq,
        uploadContainer = $("#uploadContainer"),
        spinner = $(".spinner-grow"),

        addLastInputToUploadContainer = () => {

            uploadContainer.append(
                $("<input/>", {
                    type: "file",
                    name: "files",
                    class: "form-control"
                })
            );
        },

        clearUploadContainer = () => {
            spinner.hide();
            uploadContainer.empty();
            addLastInputToUploadContainer();
        },

        initUIForm = () => {

            new DeleteBtn();
            new SubmitBtn();
            new CancelBtn();

            $("#uploadContainer").on('change', "input:last", function () {
                addLastInputToUploadContainer();
            });

            clearUploadContainer();
        },

        submit = () => {

            spinner.show();

            $
                .post({
                    url: "/api/v1/transactions",
                    data: new FormData($jq[0]),
                    enctype: 'multipart/form-data',
                    contentType: false,
                    cache: false,
                    processData: false
                })
                .done(() => {

                    $("#upload-modal").modal('hide');
                    clearUploadContainer();

                    alert('All files have been successfully uploaded to the server.' +
                        ' The result will appear as soon as the files are processed')
                })
                .fail(jqXHR => {

                    $("#upload-modal").modal('hide');
                    clearUploadContainer();
                    alert("An error occurred during the download. Part of the information may be lost");
                    logErr(jqXHR);
                });
        };

    function DeleteBtn() {
        $('#add-file-delete-last-input-btn').click(function () {

            let inputs = uploadContainer.find("input");

            if (inputs.length !== 1) {
                uploadContainer.find("input:last").remove();
            } else {
                clearUploadContainer();
            }

        });
    }

    function SubmitBtn() {
        $('#add-file-confirm-btn').click(function (event) {
            submit();
        });
    }

    function CancelBtn() {
        $('#add-file-close-btn').click(function (event) {
            clearUploadContainer();
        });
    }


    initUIForm();
}



