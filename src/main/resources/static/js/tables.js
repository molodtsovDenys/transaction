function initTable(jqTable, options) {
    if (!options) {
        options = {};
    }

    return jqTable.DataTable(options);
}

function tr() {
    return $(document.createElement("tr"));
}

function td(text) {
    return $(document.createElement("td")).text(text);
}

function htmlTd(text) {
    return $(document.createElement("td")).html(text);
}

function iconTd(styles) {
    return $(document.createElement("td"))
        .append('<i class="' + styles + '"></i>').attr("align", "center");
}