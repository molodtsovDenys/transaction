$(document).ready(function () {

    handleMenuItem($("#nav-clients"), openClientList);

    new FilesUploadForm($('#upload-files-form'));

});


function handleMenuItem(item, handler) {

    item.click(function () {

        $.each($('.active'), function (index, item) {
            $(item).removeClass('active')
        });

        $(this).parent().addClass('active');

        switchMainContent(handler);
    });
}

function switchMainContent(handler) {
    $('#main-content').empty();

    handler();
}

function logErr(jqXHR) {

    console.error("=================");
    console.error("Status: " + jqXHR.status);
    console.error("------");
    console.error(jqXHR.responseText);
    console.error("==================");
}



