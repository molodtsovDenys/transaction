function uploadTransactionList(inn) {

    $('#transactions').load(
        '/v1/transactions #transactions-main > *',
        () => {
            initTable($("#transactions-tbl"),
                {
                    processing: true,
                    serverSide: true,
                    ordering: false,
                    ajax: {
                        type: "GET",
                        url: "/api/v1/clients/" + inn + "/transactions",
                        dataType: "json",
                        data: function (data) {
                            data.searchValue = data.search.value;
                            return data;
                        }
                    },

                    columns: [
                        {data: "place"},
                        {data: "amount"},
                        {data: "currency"},
                        {data: "card"}
                    ]
                }
            )
        }
    );

}











