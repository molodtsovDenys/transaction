package com.transaction.demo.application;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.transaction.demo.port.ClientListItem;
import com.transaction.demo.port.ClientListItemContainer;
import com.transaction.demo.port.Clients;
import com.transaction.demo.port.TransactionListItem;
import com.transaction.demo.port.TransactionListItemContainer;
import com.transaction.demo.port.Transactions;
import com.transaction.demo.port.exception.ParseXmlException;
import com.transaction.demo.port.exception.ProcessClientException;
import com.transaction.demo.port.exception.ProcessTransactionException;
import com.transaction.demo.port.exception.UploadFileException;
import com.transaction.demo.port.xml.TransactionXmlList;
import com.transaction.demo.port.xml.TransactionXmlListItem;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static java.util.stream.Collectors.toUnmodifiableList;

@Component
@RequiredArgsConstructor
public class ApplicationServiceImpl implements ApplicationService {

	private final Transactions transactions;
	private final Clients clients;

	@Override
	public ClientListItemContainer getClients(
		final Integer draw,
		final Integer start,
		final Integer length,
		final String searchValue
	) {

		final var items = clients.get(searchValue);

		return new ClientListItemContainer(
			draw,
			items.size(),
			items.size(),
			items
				.parallelStream()
				.sorted(Comparator.comparing(ClientListItem::getInn))
				.skip(start)
				.limit(length)
				.collect(toUnmodifiableList()));

	}


	@Override
	public TransactionListItemContainer getTransactions(
		final String inn,
		final Integer draw,
		final Integer start,
		final Integer length,
		final String searchValue
	) {

		final var items = transactions.get(inn, searchValue);

		return new TransactionListItemContainer(
			draw,
			items.size(),
			items.size(),
			items
				.parallelStream()
				.sorted(Comparator.comparing(TransactionListItem::getCard))
				.skip(start)
				.limit(length)
				.collect(toUnmodifiableList()));
	}

	@Override
	public void upload(final InputStream stream) {

		final var items = Collections.unmodifiableList(streamToItems(stream));

		Mono
			.just(CompletableFuture.allOf(
				processClients(items),
				processTransactions(items)))
			.subscribe();
	}


	public List<TransactionXmlListItem> streamToItems(final InputStream stream) {
		try {
			return parseXml(
				stream,
				"GetTransactionsResponse",
				TransactionXmlList.class
			)
				.transactions;
		} catch (XMLStreamException e) {
			throw new ParseXmlException(e.getMessage());
		} catch (IOException e) {
			throw new UploadFileException(e.getMessage());
		}
	}

	private <T> T parseXml(final InputStream stream, final String startElement, Class<T> clazz)
		throws XMLStreamException, IOException {

		final XMLStreamReader sr = XMLInputFactory
			.newFactory()
			.createXMLStreamReader(stream);

		while (sr.hasNext()) {
			if (sr.next() == XMLStreamConstants.START_ELEMENT) {
				if (sr.getLocalName().equals(startElement)) {
					break;
				}
			}
		}

		return new XmlMapper().readValue(sr, clazz);
	}

	private CompletableFuture<Void> processClients(final List<TransactionXmlListItem> items) {
		return CompletableFuture.runAsync(() ->
			items
				.parallelStream()
				.map(item -> item.client)
				.forEach(clients::save)
		).exceptionally((Throwable t) -> {
			throw new ProcessClientException(t.getMessage());
		});
	}

	private CompletableFuture<Void> processTransactions(final List<TransactionXmlListItem> items) {
		return CompletableFuture.runAsync(() ->
			items
				.parallelStream()
				.map(this::mapToTransactionListItem)
				.forEach(transactions::save)
		).exceptionally((Throwable t) -> {
			throw new ProcessTransactionException(t.getMessage());
		});
	}

	private TransactionListItem mapToTransactionListItem(final TransactionXmlListItem item) {
		return new TransactionListItem(
			UUID.randomUUID(),
			item.client.inn,
			item.place,
			new BigDecimal(item.amount)
				.setScale(3, RoundingMode.HALF_DOWN),
			item.currency,
			item.card
		);
	}


}

