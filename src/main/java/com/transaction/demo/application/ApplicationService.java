package com.transaction.demo.application;

import com.transaction.demo.port.ClientListItemContainer;
import com.transaction.demo.port.TransactionListItemContainer;
import com.transaction.demo.port.xml.TransactionXmlListItem;

import java.io.InputStream;
import java.util.List;

public interface ApplicationService {

	ClientListItemContainer getClients(
		Integer draw,
		Integer start,
		Integer length,
		String searchValue
	);

	TransactionListItemContainer getTransactions(
		String inn,
		Integer draw,
		Integer start,
		Integer length,
		String searchValue
	);

	List<TransactionXmlListItem> streamToItems(final InputStream stream);

	void upload(final InputStream file);


}
