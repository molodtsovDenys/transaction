package com.transaction.demo.port.xml;

import com.transaction.demo.port.ClientListItem;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
public class TransactionXmlListItem {

	public String place;
	public String amount;
	public String currency;
	public String card;

	public ClientListItem client;

}
