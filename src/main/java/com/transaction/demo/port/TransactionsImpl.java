package com.transaction.demo.port;

import com.google.common.collect.ImmutableSet;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class TransactionsImpl implements Transactions {

	private final TransactionsRepo transactionsRepo;

	@Override
	public ImmutableSet<TransactionListItem> get(final String inn, final String searchValue) {
		return
			ImmutableSet.copyOf(Objects.requireNonNull((
				searchValue.isEmpty()
					? transactionsRepo.findByInn(inn)
					: getBySearchValueFor(inn, searchValue))
				.collectList().block()));
	}

	public Flux<TransactionListItem> getBySearchValueFor(final String inn, final String searchValue) {

		try {
			return
				Flux.concat(
					transactionsRepo.findByInnAndPlaceLike(inn, searchValue),
					transactionsRepo.findByInnAndAmountLike(inn, new BigDecimal(searchValue)),
					transactionsRepo.findByInnAndCurrencyLike(inn, searchValue),
					transactionsRepo.findByInnAndCardLike(inn, searchValue));

		} catch (Exception e) {
			return
				Flux.concat(
					transactionsRepo.findByInnAndPlaceLike(inn, searchValue),
					transactionsRepo.findByInnAndCurrencyLike(inn, searchValue),
					transactionsRepo.findByInnAndCardLike(inn, searchValue));
		}
	}

	@Override
	public void save(final TransactionListItem item) {
		final Mono<TransactionListItem> result = transactionsRepo.save(item);
		result.subscribe();
	}


}
