package com.transaction.demo.port;

import java.util.Set;

public interface Transactions {

	Set<TransactionListItem> get(String inn, String searchValue);

	void save(TransactionListItem item);


}
