package com.transaction.demo.port;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class ClientListItemContainer {
	public final int draw;
	public final int recordsTotal;
	public final int recordsFiltered;
	public final List<ClientListItem> data;
}
