package com.transaction.demo.port;

import java.util.Set;

public interface Clients {

	Set<ClientListItem> get(String searchValue);

	void save(ClientListItem item);
}
