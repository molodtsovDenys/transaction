package com.transaction.demo.port;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class TransactionListItemContainer {
	public final int draw;
	public final int recordsTotal;
	public final int recordsFiltered;
	public final List<TransactionListItem> data;

}
