package com.transaction.demo.port;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

import java.math.BigDecimal;
import java.util.UUID;

public interface TransactionsRepo extends ReactiveMongoRepository<TransactionListItem, UUID> {

	Flux<TransactionListItem> findByInn(String inn);

	Flux<TransactionListItem> findByInnAndPlaceLike(String inn, String request);

	Flux<TransactionListItem> findByInnAndAmountLike(String inn, BigDecimal request);

	Flux<TransactionListItem> findByInnAndCurrencyLike(String inn, String request);

	Flux<TransactionListItem> findByInnAndCardLike(String inn, String request);

}
