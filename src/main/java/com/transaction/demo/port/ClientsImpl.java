package com.transaction.demo.port;

import com.google.common.collect.ImmutableSet;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Component
@RequiredArgsConstructor
public class ClientsImpl implements Clients {

	private final ClientsRepo clientsRepo;

	@Override
	public ImmutableSet<ClientListItem> get(final String searchValue) {
		return
			ImmutableSet.copyOf(Objects.requireNonNull((
				searchValue.isEmpty()
					? clientsRepo.findAll()
					: getBySearchValue(searchValue))
				.collectList().block()));
	}

	public Flux<ClientListItem> getBySearchValue(final String searchValue) {
		return
			Flux.concat(
				clientsRepo.findByFirstNameLike(searchValue),
				clientsRepo.findByLastNameLike(searchValue),
				clientsRepo.findByMiddleNameLike(searchValue),
				clientsRepo.findByInnLike(searchValue));
	}

	@Override
	public void save(final ClientListItem item) {
		final Mono<ClientListItem> result = clientsRepo.save(item);
		result.subscribe();
	}
}
