package com.transaction.demo.port;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class UIControllerV1 {

	@GetMapping("/")
	public String index() {
		return "index";
	}

	@GetMapping("/v1/clients")
	public String clients() {
		return "clients";
	}

	@GetMapping("/v1/transactions")
	public String transactions() {
		return "transactions";
	}


}

