package com.transaction.demo.port.exception;

public class ProcessClientException extends RuntimeException {
	public ProcessClientException(String message) {
		super(message);
	}
}
