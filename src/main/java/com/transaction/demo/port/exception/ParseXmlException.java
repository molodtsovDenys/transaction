package com.transaction.demo.port.exception;

public class ParseXmlException extends RuntimeException {
	public ParseXmlException(String message) {
		super(message);
	}
}
