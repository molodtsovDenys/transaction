package com.transaction.demo.port.exception;

public class ProcessTransactionException extends RuntimeException {
	public ProcessTransactionException(String message) {
		super(message);
	}
}
