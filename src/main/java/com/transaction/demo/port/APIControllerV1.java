package com.transaction.demo.port;


import com.transaction.demo.application.ApplicationService;
import com.transaction.demo.port.exception.UploadFileException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.Arrays;


@RestController
@RequestMapping(value = "/api/v1/")
@RequiredArgsConstructor
public class APIControllerV1 {

	private final ApplicationService application;


	@GetMapping("clients")
	public Mono<ClientListItemContainer> clientList(
		@RequestParam final Integer draw,
		@RequestParam final Integer start,
		@RequestParam final Integer length,
		@RequestParam final String searchValue
	) {

		return Mono.just(application.getClients(draw, start, length, searchValue));
	}

	@GetMapping("clients/{inn}/transactions")
	public Mono<TransactionListItemContainer> transactionListByClient(
		@PathVariable final String inn,
		@RequestParam final Integer draw,
		@RequestParam final Integer start,
		@RequestParam final Integer length,
		@RequestParam final String searchValue

	) {
		return Mono.just(application.getTransactions(inn, draw, start, length, searchValue));
	}


	@PostMapping("transactions")
	public HttpStatus upload(
		@RequestParam("files") final MultipartFile[] request
	) {

		Mono.just(
			Arrays.asList(request)
				.parallelStream()
				.filter(file -> file.getSize() > 0)
				.filter(file -> file.getOriginalFilename().endsWith(".xml"))
				.map(file -> {
					try {
						return file.getInputStream();
					} catch (IOException e) {
						throw new UploadFileException(file.getOriginalFilename());
					}
				}))
			.subscribe(items -> items.forEach(application::upload));

		return HttpStatus.OK;
	}

}