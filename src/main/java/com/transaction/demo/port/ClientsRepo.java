package com.transaction.demo.port;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface ClientsRepo extends ReactiveMongoRepository<ClientListItem, String> {

	Flux<ClientListItem> findByInnLike(String request);

	Flux<ClientListItem> findByFirstNameLike(String request);

	Flux<ClientListItem> findByLastNameLike(String request);

	Flux<ClientListItem> findByMiddleNameLike(String request);
}
