package com.transaction.demo.port;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.spy;

@DataMongoTest
@RunWith(SpringRunner.class)
public class ClientsTest_IT {

	private ClientsImpl clients;

	@Autowired
	private ClientsRepo clientsRepo;

	Publisher<ClientListItem> setup;
	private ClientListItem ivan;
	private ClientListItem sidr;

	@Before
	public void setUp() {

		clients = spy(new ClientsImpl(clientsRepo));

		ivan = ClientListItem.builder()
			.inn("1")
			.firstName("Ivan")
			.lastName("Ivanov")
			.middleName("Ivanovich")
			.build();

		sidr = ClientListItem.builder()
			.inn("2")
			.firstName("Sidr")
			.lastName("Sidorov")
			.middleName("Sidorovich")
			.build();

		setup =
			clientsRepo
				.deleteAll()
				.thenMany(clientsRepo.saveAll(Flux.just(ivan, sidr)));

	}

	@Test
	public void save() {

		clients.save(ClientListItem.builder()
			.inn("5")
			.firstName("Fedor")
			.lastName("F")
			.middleName("FF")
			.build());

		ClientListItem actual = clients.getBySearchValue("5").blockFirst();

		assertThat(actual, is(not(nullValue())));
		assertThat(actual.inn, is("5"));
		assertThat(actual.firstName, is("Fedor"));

	}


	@Test
	public void get_findByInnLike_Return_1() {

		var find = clients.getBySearchValue("1");

		var composite = Flux
			.from(setup)
			.thenMany(find);

		StepVerifier
			.create(composite)
			.expectNext(ivan)
			.expectNextCount(0)
			.verifyComplete();

	}

	@Test
	public void get_findBySearchValue_Return_3() {

		var find = clients.getBySearchValue("Ivan");

		var composite = Flux
			.from(setup)
			.thenMany(find);

		StepVerifier
			.create(composite)
			.expectNextCount(3)
			.expectNextCount(0)
			.verifyComplete();
	}

	@Test
	public void get_findByWrongSearchValue_Return_0() {

		var find = clients.getBySearchValue("dfsdfsf");

		var composite = Flux
			.from(setup)
			.thenMany(find);

		StepVerifier
			.create(composite)
			.expectNextCount(0)
			.verifyComplete();
	}


}