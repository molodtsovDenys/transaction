package com.transaction.demo.port;

import lombok.RequiredArgsConstructor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.math.BigDecimal;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.spy;


@DataMongoTest
@RunWith(SpringJUnit4ClassRunner.class)
@RequiredArgsConstructor
public class TransactionsTest_IT {

	TransactionsImpl transactions;


	@Autowired
	private TransactionsRepo transactionsRepo;

	private Publisher<TransactionListItem> setup;

	private String one_INN;
	private String one_PLACE;
	private String one_CARD;
	private String one_CURRENCY;
	private TransactionListItem one;
	private TransactionListItem two;

	private String three_INN;
	private String three_PLACE;
	private String three_CARD;
	private String three_CURRENCY;
	private TransactionListItem three;


	@Before
	public void setUp() {

		transactions = spy(new TransactionsImpl(transactionsRepo));

		one_INN = "1";
		one_PLACE = "pl-1";
		one_CARD = "card-1";
		one_CURRENCY = "USD";

		three_INN = "3";
		three_PLACE = "pl-3";
		three_CARD = "card-3";
		three_CURRENCY = "UAH";

		one = TransactionListItem.builder()
			.id(UUID.randomUUID())
			.inn(one_INN)
			.place(one_PLACE)
			.amount(BigDecimal.ONE)
			.currency(one_CURRENCY)
			.card(one_CARD)
			.build();

		two = TransactionListItem.builder()
			.id(UUID.randomUUID())
			.inn(one_INN)
			.place(one_PLACE)
			.amount(BigDecimal.ONE)
			.currency(one_CURRENCY)
			.card(one_CARD)
			.build();

		three = TransactionListItem.builder()
			.id(UUID.randomUUID())
			.inn(three_INN)
			.place(three_PLACE)
			.amount(BigDecimal.ONE)
			.currency(three_CURRENCY)
			.card(three_CARD)
			.build();

		setup =
			transactionsRepo
				.deleteAll()
				.thenMany(transactionsRepo.saveAll(Flux.just(one, two, three)));

	}

	@Test
	public void save() {

		transactions.save(TransactionListItem.builder()
			.id(UUID.randomUUID())
			.inn("555")
			.place(one_PLACE)
			.amount(BigDecimal.ONE)
			.currency(one_CURRENCY)
			.card("CARD-100")
			.build());

		TransactionListItem actual = transactions
			.getBySearchValueFor("555", "").blockFirst();

		assertThat(actual, is(not(nullValue())));
		assertThat(actual.inn, is("555"));
		assertThat(actual.card, is("CARD-100"));
	}


	@Test
	public void get_findByInn_ShouldFetch_2_Items() {

		var find = transactions.getBySearchValueFor(one_INN, one_PLACE);

		var composite = Flux
			.from(setup)
			.thenMany(find);

		StepVerifier
			.create(composite)
			.expectNext(one, two)
			.expectNextCount(0)
			.verifyComplete();

	}

	@Test
	public void get_findByInn_ShouldFetch_1_Item() {

		var find = transactions.getBySearchValueFor(three_INN, three_PLACE);

		var composite = Flux
			.from(setup)
			.thenMany(find);

		StepVerifier
			.create(composite)
			.expectNext(three)
			.expectNextCount(0)
			.verifyComplete();
	}


	@Test
	public void get_findByInnAndWrongSearchValue_Return_0() {

		var find = transactions.getBySearchValueFor(three_INN, one_PLACE);

		var composite = Flux
			.from(setup)
			.thenMany(find);

		StepVerifier
			.create(composite)
			.expectNextCount(0)
			.verifyComplete();
	}

	@Test
	public void get_findByWrongInnAndEmptySearchValue_Return_0() {

		var find = transactions.getBySearchValueFor("2", "");

		var composite = Flux
			.from(setup)
			.thenMany(find);

		StepVerifier
			.create(composite)
			.expectNextCount(0)
			.verifyComplete();

	}

	@Test
	public void get_findByWrongInnAndWrongSearchValue_Return_0() {

		var find = transactions.getBySearchValueFor("100", "100");

		var composite = Flux
			.from(setup)
			.thenMany(find);

		StepVerifier
			.create(composite)
			.expectNextCount(0)
			.verifyComplete();

	}


}