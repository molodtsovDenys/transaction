package com.transaction.demo.application;

import com.transaction.demo.port.ClientListItem;
import com.transaction.demo.port.Clients;
import com.transaction.demo.port.TransactionListItem;
import com.transaction.demo.port.Transactions;
import com.transaction.demo.port.xml.TransactionXmlListItem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationServiceImplTest {

	@Spy
	@InjectMocks
	private ApplicationServiceImpl application;

	@Mock
	private Clients clients;

	@Mock
	private Transactions transactions;

	@Test
	public void getClients() {
//given
		Set<ClientListItem> clientListItems = new HashSet<>() {{

			for (int i = 0; i < 20; i++) {
				int j = i + 10;
				add(
					ClientListItem.builder()
						.inn(String.valueOf(j))
						.firstName("FirstName-" + j)
						.lastName("LastName-" + j)
						.middleName("MiddleName-" + j)
						.build()
				);
			}

		}};
//when
		when(clients.get(anyString())).thenReturn(clientListItems);

		final int CONTAINER_SIZE = 10;

		var container = application.getClients(1, 0, CONTAINER_SIZE, "");

//then
		assertEquals(CONTAINER_SIZE, container.data.size());

		assertEquals(1, container.draw);
		assertEquals(20, container.recordsFiltered);
		assertEquals(20, container.recordsTotal);

		for (int i = 0; i < CONTAINER_SIZE; i++) {

			var client = container.data.get(i);
			int j = i + 10;

			assertEquals(String.valueOf(j), client.inn);
			assertEquals("FirstName-" + j, client.firstName);
			assertEquals("LastName-" + j, client.lastName);
			assertEquals("MiddleName-" + j, client.middleName);
		}

	}


	@Test
	public void getTransactions() {
//given
		Set<TransactionListItem> transactionListItems = new HashSet<>() {{

			for (int i = 0; i < 20; i++) {
				add(
					TransactionListItem.builder()
						.id(UUID.randomUUID())
						.inn("1")
						.place("pl-" + i)
						.amount(BigDecimal.ONE)
						.currency("USD")
						.card("card-" + i)
						.build()
				);
			}

		}};
//when
		when(transactions.get(anyString(), anyString())).thenReturn(transactionListItems);

		var container = application.getTransactions("1", 1, 0, 15, "");
//then
		assertEquals(1, container.draw);
		assertEquals(20, container.recordsFiltered);
		assertEquals(20, container.recordsTotal);
		assertEquals(15, container.data.size());

	}

	@Test
	public void streamToItems_ParseXmlFile_ShouldBeConsistent() {
//given
		String xml = "<?xml version='1.0' encoding='UTF-8'?>\n" +
			"<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body>\n" +
			"        <ns2:GetTransactionsResponse xmlns:ns2='http://dbo.qulix.com/ukrsibdbo'>\n" +
			"<transactions>\n" +
			"    <transaction>\n" +
			"        <place>Place 13000</place>\n" +
			"        <amount>71471.653</amount>\n" +
			"        <currency>USD</currency>\n" +
			"        <card>123456****510075</card>\n" +
			"        <client>\n" +
			"            <inn>13000</inn>\n" +
			"            <firstName>Egor</firstName>\n" +
			"            <lastName>Abrashkin</lastName>\n" +
			"            <middleName>Kozemirovich</middleName>\n" +
			"        </client>\n" +
			"    </transaction>\n" +
			"</transactions>\n" +
			" </ns2:GetTransactionsResponse></soap:Body></soap:Envelope>";

//when
		List<TransactionXmlListItem> transactionXmlListItems =
			application.streamToItems(new ByteArrayInputStream(xml.getBytes()));

		TransactionXmlListItem transaction = transactionXmlListItems.get(0);

		ClientListItem client = transaction.client;

//then
		assertThat(transactionXmlListItems, is(not(nullValue())));
		assertThat(transactionXmlListItems.size(), is(1));

		assertThat(client, is(not(nullValue())));
		assertThat(client.inn, is("13000"));
		assertThat(client.firstName, is("Egor"));
		assertThat(client.lastName, is("Abrashkin"));
		assertThat(client.middleName, is("Kozemirovich"));

		assertThat(transaction, is(not(nullValue())));
		assertThat(transaction.client, is(client));
		assertThat(transaction.place, is("Place 13000"));
		assertThat(transaction.amount, is("71471.653"));
		assertThat(new BigDecimal(transaction.amount), is(not(nullValue())));
		assertThat(transaction.currency, is("USD"));
		assertThat(transaction.card, is("123456****510075"));


	}


}