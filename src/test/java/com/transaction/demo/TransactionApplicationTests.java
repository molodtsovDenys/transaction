package com.transaction.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionApplicationTests {

	@Autowired
	private WebApplicationContext ctx;

	@Test
	public void contextLoads() {
		ServletContext servletContext = ctx.getServletContext();
		assertNotNull(servletContext);
	}


}
